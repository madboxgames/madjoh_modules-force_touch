define([
	'require',
	'madjoh_modules/styling/styling',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/wording/wording'
],
function(require, Styling, CustomEvents, Wording){
	var ForceTouch = {
		init : function(ForceTouchSettings){
			if(!window.cordova || !window.ThreeDeeTouch || Styling.isAndroid()) return;

			ThreeDeeTouch.isAvailable(function(available){
				if(!available) return;

				var actions = [];
				for(var i = 0; i < ForceTouchSettings.length; i++) actions.push({type : ForceTouchSettings[i].pageKey, title : Wording.getText(ForceTouchSettings[i].titleKey)});

				ThreeDeeTouch.configureQuickActions(actions);
				ThreeDeeTouch.onHomeIconPressed = function(payload){
					for(var i = 0; i < actions.length; i++){
						if(payload.type === actions[i].type) CustomEvents.fireCustomEvent(document, 'ChangePage', {key : actions[i].type, stack : true});
					}
				};
			});
		}
	};

	return ForceTouch;
});
